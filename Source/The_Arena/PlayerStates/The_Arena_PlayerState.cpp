// Fill out your copyright notice in the Description page of Project Settings.


#include "The_Arena_PlayerState.h"


void AThe_Arena_PlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AThe_Arena_PlayerState, CurrentTeam);
}

void AThe_Arena_PlayerState::CopyProperties(APlayerState* PlayerState)
{
	Super::CopyProperties(PlayerState);

	if(auto ArenaPlayerState = Cast<AThe_Arena_PlayerState>(PlayerState))
	{
		ArenaPlayerState->SetCurrentTeam(GetCurrentTeam());
	}
}

void AThe_Arena_PlayerState::OnRep_CurrentTeam()
{
	CurrentTeamDispatcher.Broadcast();
}

void AThe_Arena_PlayerState::SetCurrentTeam(EGameTeam Team)
{
	CurrentTeam = Team;
}

EGameTeam AThe_Arena_PlayerState::GetCurrentTeam() const
{
	return CurrentTeam;
}

FName AThe_Arena_PlayerState::GetCurrentTeamName() const
{
	FName TeamName = "NONE";

	switch (CurrentTeam)
	{
	case EGameTeam::Team_1:
		TeamName = "Team_1";
		break;
	case EGameTeam::Team_2:
		TeamName = "Team_2";
		break;
	case EGameTeam::Spectators:
		TeamName = "Spectators";
		break;
	default:
		break;
	}

	return TeamName;
}

FDMD& AThe_Arena_PlayerState::GetCurrentTeamDispatcher()
{
	return CurrentTeamDispatcher;
}
