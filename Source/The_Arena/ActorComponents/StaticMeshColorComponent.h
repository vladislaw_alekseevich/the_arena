// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "StaticMeshColorComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THE_ARENA_API UStaticMeshColorComponent : public UActorComponent
{
	GENERATED_BODY()

	//Methods
public:	
	// Sets default values for this component's properties
	UStaticMeshColorComponent();
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	FColor GetStaticMeshColor() const;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	//Fields
protected:
	UPROPERTY(EditAnywhere)
	FColor StaticMeshColor;
};
