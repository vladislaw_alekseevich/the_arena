// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "The_Arena/ActorComponents/DragAndDropManagerComponent.h"

#include "InventoryBaseComponent.generated.h"


UENUM()
enum class ERemoveItemOption : uint8 { WithSave, WithoutSave };

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THE_ARENA_API UInventoryBaseComponent : public UActorComponent
{
	GENERATED_BODY()

	//Methods
public:	
	// Sets default values for this component's properties
	UInventoryBaseComponent();
	
	virtual void RemoveItemFromBaseInventory(AInventoryItemObject* InventoryItemObject);
	virtual void DragItem(AInventoryItemObject* InventoryItemObject, UDragAndDropManagerComponent* DragAndDropComponent);
	virtual void DropItem(UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount);
	virtual void DropItemAt(int TileIndex, UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount);
	virtual bool CanUsed(AActor* ActorUser);
protected:
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//Fields
	UPROPERTY(EditDefaultsOnly)
	float InteractionDistance = 500.0f;
};
