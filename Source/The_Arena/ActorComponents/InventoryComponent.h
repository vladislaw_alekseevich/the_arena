// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "The_Arena/Actors/InventoryItemObject.h"
#include <functional>

#include "The_Arena/ActorComponents/InventoryBaseComponent.h"
#include "The_Arena/UObjects/DefaultDelegates.h"

#include "InventoryComponent.generated.h"

USTRUCT(BlueprintType)
struct FLine
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	FVector2D Start;
	UPROPERTY(BlueprintReadWrite)
	FVector2D End;
};

USTRUCT(BlueprintType)
struct FTile
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	int X;
	UPROPERTY(BlueprintReadWrite)
	int Y;
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THE_ARENA_API UInventoryComponent : public UInventoryBaseComponent
{
	GENERATED_BODY()

	//Methods
public:	
	// Sets default values for this component's properties
	UInventoryComponent();

	UFUNCTION(Server, Reliable, BlueprintCallable)
		void ServerOpenInventory();
	UFUNCTION(Server, Reliable, BlueprintCallable)
		void ServerCloseInventory();
	
	void RemoveItemFromBaseInventory(AInventoryItemObject* InventoryItemObject) override;	
	void DragItem(AInventoryItemObject* InventoryItemObject, UDragAndDropManagerComponent* DragAndDropComponent) override;
	void DropItem(UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount) override;
	void DropItemAt(int TopLeftIndex, UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount) override;
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int GetColumns() const;
	UFUNCTION(BlueprintCallable)
	void SetColumns(int InColumns);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	int GetRows() const;
	UFUNCTION(BlueprintCallable)
	void SetRows(int InRows);
	
	UFUNCTION(BlueprintCallable)
	virtual bool TryAddItem(AInventoryItemObject* InventoryItemObject, bool bWithItemAmount);
	UFUNCTION(BlueprintCallable)
	virtual bool TryAddItemAt(AInventoryItemObject* InventoryItemObject, int TopLeftIndex, bool bWithItemAmount);
	UFUNCTION(BlueprintCallable)
	virtual void RemoveItem(AInventoryItemObject* InventoryItemObject, ERemoveItemOption RemoveItemOption);
	
	UFUNCTION(BlueprintCallable)
	virtual bool IsRoomAvailable(AInventoryItemObject* InventoryItemObject, int TopLeftIndex, bool bWithItemAmount);
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsAttachmentAvailable(int Index, AInventoryItemObject* InventoryItemObject);
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FTile IndexToTile(int Index);
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int TileToIndex(FTile Tile);
	UFUNCTION(BlueprintCallable, BlueprintPure)
	TMap<AInventoryItemObject*, FTile> GetAllItems();
	UFUNCTION(BlueprintCallable)
	bool Contains(AInventoryItemObject* InventoryItemObject);
	UFUNCTION(BlueprintCallable)
	int IndexOfItem(AInventoryItemObject* InventoryItemObject);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetCommonItemsWeight() const;

	FDMD& GetInventoryChangedDispatcher();
	FDMD& GetCommonItemsWeightDispatcher();
	FDMD& GetInventoryOpenedDispatcher();
	FDMD_PC& GetInventoryOpened_PC_Dispatcher();
	FDMD& GetInventoryClosedDispathcer();
	FDMD_PC& GetInventoryClosed_PC_Dispatcher();
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void AddItemAt(AInventoryItemObject* InventoryItemObject, int TopLeftIndex, bool bWithItemAmount);
	void RemoveItemInternal(AInventoryItemObject* InventoryItemObject, ERemoveItemOption RemoveItemOption);
	
	bool IsRoomAvailableInternal(int SizeX, int SizeY, int TopLeftIndex, std::function<bool(int)> AdditionalCondition = nullptr);
	bool IsRoomAvailableInternal(int SizeX, int SizeY, int TopLeftIndex, std::function<bool(UInventoryComponent*, int)> AdditionalCondition = nullptr);

	bool CheckOnConsideringItemLambda(int Index);
	bool CheckOnConsideringAndEqualityLambda(int Index, AInventoryItemObject* InventoryItemObject);
	
	UFUNCTION()
	virtual void UpdateOnCurrentItemAmount();
	UFUNCTION()
	virtual void UpdateOnAttachModule();
	
	UFUNCTION()
	void UpdateCommonItemsWeight();
	
	UFUNCTION()
	void OnRep_Items();
	UFUNCTION()
	void OnInventoryChanged();
	UFUNCTION()
	void OnRep_CommonItemsWeight();

	UFUNCTION(NetMulticast, Reliable)
	void MulticastOnInventoryOpened();
	UFUNCTION(NetMulticast, Reliable)
	void MulticastOnInventoryClosed();
	
	//Fields
protected:
	UPROPERTY(EditDefaultsOnly, Replicated)
	int Columns;
	UPROPERTY(EditDefaultsOnly, Replicated)
	int Rows;
	
	UPROPERTY(ReplicatedUsing = OnRep_Items)
	TArray<AInventoryItemObject*> Items;

	UPROPERTY(ReplicatedUsing = OnRep_CommonItemsWeight)
	float CommonItemsWeight;
	
	//Events
	UPROPERTY(BlueprintAssignable)
	FDMD InventoryChangedDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD CommonItemsWeightDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD InventoryOpenedDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD_PC InventoryOpened_PC_Dispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD InventoryClosedDispathcer;
	UPROPERTY(BlueprintAssignable)
	FDMD_PC InventoryClosed_PC_Dispatcher;
};