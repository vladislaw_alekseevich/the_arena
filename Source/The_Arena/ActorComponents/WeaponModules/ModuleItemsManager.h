// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "The_Arena/ActorComponents/WeaponModules/MotherConnectorComponent.h"

#include "ModuleItemsManager.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THE_ARENA_API UModuleItemsManager : public UActorComponent
{
	GENERATED_BODY()

	//Methods
public:
	bool TryAttachModuleToMotherConnector(AActor* Module, UMotherConnectorComponent* MotherConnector);
	bool TryDetachModuleFromMotherConnector(AActor* Module, UMotherConnectorComponent* MotherConnector);
	bool CanModuleAttachedToMotherConnector(AActor* Module, UMotherConnectorComponent* MotherConnector);
	
	//This is not entirely correct, but for this architecture is fine.
	UFUNCTION(BlueprintCallable)
	bool TryAttachModule(AActor* Module);
	UFUNCTION(BlueprintCallable)
	bool TryDetachModule(AActor* Module);
	UFUNCTION(BlueprintCallable)
	bool CanModuleAttached(AActor* Module);
	//
	
	TArray<UMotherConnectorComponent*> GetMotherConnectors() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	TArray<AActor*> GetAttachedModuleItems() const;
	
protected:
	UModuleItemsManager();
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void Initialization();
	//Fields
protected:	
	UPROPERTY()
	TArray<UMotherConnectorComponent*> MotherConnectors;
};
