// Fill out your copyright notice in the Description page of Project Settings.


#include "MotherConnectorComponent.h"
#include "Net/UnrealNetwork.h"

#include "The_Arena/ActorComponents/WeaponModules/FatherConnectorComponent.h"
#include "The_Arena/Actors/Weapons/RifleBase.h"

// Sets default values for this component's properties
UMotherConnectorComponent::UMotherConnectorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UMotherConnectorComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UMotherConnectorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UMotherConnectorComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UMotherConnectorComponent, AttachedModule);
	DOREPLIFETIME(UMotherConnectorComponent, AttachedFatherConnector);
}

void UMotherConnectorComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if(GetOwnerRole() == ROLE_Authority)
	{
		if (AttachedModule)
		{
			AttachedModule->Destroy();
		}
	}
	
	Super::EndPlay(EndPlayReason);
}

FDMD& UMotherConnectorComponent::GetSetupAttachmentDispatcher()
{
	return SetupAttachmentDispatcher;
}

FDMD& UMotherConnectorComponent::GetRemoveAttachmentDispatcher()
{
	return RemoveAttachmentDispatcher;
}

TArray<TSubclassOf<UFatherConnectorComponent>> UMotherConnectorComponent::GetAcceptedFatherConnectorClasses() const
{
	return AcceptedFatherConnectorClasses;
}

void UMotherConnectorComponent::SetupAttachment(UFatherConnectorComponent* FatherConnector)
{
	if (!FatherConnector)
		return;

	auto FatherOwner = FatherConnector->GetOwner();

	if(auto ModuleItem = Cast<AActor>(FatherOwner))
	{
		AttachedFatherConnector = FatherConnector;
		AttachedModule = ModuleItem;

		SetupAttachmentDispatcher.Broadcast();
	}
}

void UMotherConnectorComponent::RemoveAttachment()
{
	AttachedFatherConnector = nullptr;
	AttachedModule = nullptr;

	RemoveAttachmentDispatcher.Broadcast();
}

AActor* UMotherConnectorComponent::GetAttachedModule() const
{
	return AttachedModule;
}

UFatherConnectorComponent* UMotherConnectorComponent::GetAttachedFatherConnector() const
{
	return AttachedFatherConnector;
}
