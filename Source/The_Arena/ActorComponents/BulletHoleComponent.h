// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Components/StaticMeshComponent.h"
#include "Landscape.h"

#include "The_Arena/UObjects/DataAssets/BulletHoleDataAsset.h"
#include "The_Arena/ActorComponents/StaticMeshColorComponent.h"

#include "BulletHoleComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THE_ARENA_API UBulletHoleComponent : public UActorComponent
{
	GENERATED_BODY()

	//Methods
public:	
	// Sets default values for this component's properties
	UBulletHoleComponent();
	
	UFUNCTION()
	void CreateBulletHole(FHitResult& OutHit);
	UFUNCTION(Server, Unreliable)
	void ServerCreateBulletHole(FHitResult OutHit);
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastCreateBulletHole(FHitResult OutHit);
	
	void PopBulletHole();
	void ClearBulletHoleArray();
	static void PopBulletHoleStatic();
	static void ClearBulletHoleArrayStatic();	
protected:
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void CreateDecal(FHitResult& OutHit, FBulletHoleParam& Param);
	void CreateParticle(FHitResult& OutHit, FBulletHoleParam& Param);
	void EnqueueDecal(UDecalComponent* Decal);
	void PlayBulletHoleSound(FHitResult& OutHit, FBulletHoleParam& Param);
	FColor GetStaticMeshColor(UStaticMeshComponent* MeshComp);
	FColor GetLandscapeColor(AActor* Landscape);
	static void AttachFunctionToChangeLevelDelegate(ULevel* Level, UWorld* World);
	static void AttachFunctionToWorldCleanupDelegate( UWorld* World, bool SessionEnded, bool CleanupResources);
	//Fields
protected:
	UPROPERTY(EditDefaultsOnly)
	UBulletHoleDataAsset* BulletHoleDataAsset;

	static TArray<UDecalComponent*> Decals;
	static bool AttachedToChangeLevelDelegate;
	static bool AttachedToWorldCleanupDelegate;
};
