// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "The_Arena/ActorComponents/BasketBaseComponent.h"
#include "The_Arena/ActorComponents/TraderInventoryComponent.h"
#include "The_Arena/ActorComponents/MoneyComponent.h"
#include "The_Arena/ActorComponents/CustomInventoryComponent.h"

#include "TraderBasketComponent.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class THE_ARENA_API UTraderBasketComponent : public UBasketBaseComponent
{
	GENERATED_BODY()

	//Methods
public:
	void Initialization(UTraderInventoryComponent* InTraderInventoryComponent, UCustomInventoryComponent* InCustomInventoryComponent, UMoneyComponent* InMoneyComponent);
	
	void DropItem(UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount) override;
	void DropItemAt(int TopLeftIndex, UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount) override;

	bool CanUsed(AActor* ActorUser) override;

	void Buy();
	
	//Fields
protected:
	UPROPERTY()
	UTraderInventoryComponent* TraderInventoryComponent;
	UPROPERTY()
	UCustomInventoryComponent* PlayerInventoryComponent;
	UPROPERTY()
	UMoneyComponent* PlayerMoneyComponent;
	

};
