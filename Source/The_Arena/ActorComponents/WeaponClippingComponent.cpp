// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponClippingComponent.h"

#include "The_Arena/Actors/Weapons/RifleBase.h"
#include "The_Arena/Characters/FirstPersonCharacter.h"


float UWeaponClippingComponent::TimeOfAbilityToFireAfterClipping = 0.1f;

// Sets default values for this component's properties
UWeaponClippingComponent::UWeaponClippingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

void UWeaponClippingComponent::OnStartClipping_Implementation()
{
	StartClippingDispatcher.Broadcast();
	DoOnceAfterPutDownTheWeapon = true;
}

void UWeaponClippingComponent::OnPutDownWeapon_Implementation()
{
	PutDownWeaponDispatcher.Broadcast();
	DoOnceAfterPutDownTheWeapon = false;
}

UTransformController* UWeaponClippingComponent::GetClippingTransformController() const
{
	return ClippingTransformController;
}

void UWeaponClippingComponent::SetClippingTransformController(UTransformController* Controller)
{
	ClippingTransformController = Controller;
}

USceneComponent* UWeaponClippingComponent::GetEndOfWeaponComp() const
{
	return EndOfWeaponComp;
}

USceneComponent* UWeaponClippingComponent::GetEndOfWeaponInFPAreaComp() const
{
	return EndOfWeaponInFPAreaComp;
}

USceneComponent* UWeaponClippingComponent::GetFPHandsComponent() const
{
	return FPHandsComp;
}

void UWeaponClippingComponent::SetFPHandsComponent(USceneComponent* Component)
{
	FPHandsComp = Component;

	if (ClippingTransformController)
	{
		ClippingTransformController->SetSceneComp(Component);
	}
}

bool UWeaponClippingComponent::IsClipping() const
{
	return Clipping;
}

void UWeaponClippingComponent::OnComponentDestroyed(bool bDestroyingHierarchy)
{
	if (EndOfWeaponInFPAreaComp)
		EndOfWeaponInFPAreaComp->DestroyComponent();

	Super::OnComponentDestroyed(bDestroyingHierarchy);
}

void UWeaponClippingComponent::InitializationParameters(AFirstPersonCharacter* FPOwner, USceneComponent* EndOfWeapon)
{
	if (!FPOwner)
		return;

	FPCamera = FPOwner->GetFirstPersonCamera();
	SetFPHandsComponent(FPOwner->GetFirstPersonSkeletalMesh());
	EndOfWeaponComp = EndOfWeapon;

	EndOfWeaponInFPAreaComp = NewObject<USceneComponent>(FPOwner, "EndOfWeapon");
	EndOfWeaponInFPAreaComp->AttachToComponent(FPOwner->GetCapsuleComponent(), FAttachmentTransformRules::KeepRelativeTransform);

	ClippingLineTraceParams.AddIgnoredActor(FPOwner);

	if (GetOwner())
		ClippingLineTraceParams.AddIgnoredActor(GetOwner());

	DistanceToClipToForwardLocationOffsetRatio = DistanceToClip / fabsf(ForwardLocationOffset);
	PitchRotationRatio = PitchRotationOffset / DistanceToClip;
	UpLocationRatio = UpLocationOffset / DistanceToClip;
}

bool UWeaponClippingComponent::GetDoOnceAfterPutDownTheWeapon() const
{
	return DoOnceAfterPutDownTheWeapon;
}

float UWeaponClippingComponent::GetTimeOfAbilityToFireAfterClipping()
{
	return TimeOfAbilityToFireAfterClipping;
}

void UWeaponClippingComponent::SetTimeOfAbilityToFireAfterClipping(float Time)
{
	TimeOfAbilityToFireAfterClipping = Time;
}

// Called when the game starts
void UWeaponClippingComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...

}


// Called every frame
void UWeaponClippingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UpdateEndOfWeaponInFPArea();
	CheckOnClipping();
	UpdateAbilityToFire(DeltaTime);
}

void UWeaponClippingComponent::CheckOnClipping()
{
	if (!EndOfWeaponInFPAreaComp)
		return;
	
	FVector Start = EndOfWeaponInFPAreaComp->GetComponentLocation();
	FVector End = Start + FPCamera->GetForwardVector() * DistanceToClip;

	FHitResult Result;
	GetWorld()->LineTraceSingleByChannel(Result, Start, End, ECC_Visibility, ClippingLineTraceParams);

	if (auto FiringWeapon = Cast<ARifleBase>(GetOwner()))
	{
		if (Result.bBlockingHit && Result.Distance <= DistanceToClip && FiringWeapon->IsInHandsInitialized())
		{
			if (!IsClipping())
			{
				Clipping = true;
				OnStartClipping();
			}

			if (ClippingTransformController)
			{
				ClippingTransformController->SetScaleOfMoving(1);
				ClippingTransformController->Forward.Location = ForwardLocationOffset + Result.Distance / DistanceToClipToForwardLocationOffsetRatio;
				ClippingTransformController->Pitch.Rotation = PitchRotationOffset - Result.Distance * PitchRotationRatio;
				ClippingTransformController->Up.Location = UpLocationOffset - Result.Distance * UpLocationRatio;
			}
		}
		else
		{
			if (IsClipping())
			{
				Clipping = false;
			}

			if (ClippingTransformController)
			{
				ClippingTransformController->SetScaleOfMoving(0);
			}
		}
	}
}

void UWeaponClippingComponent::UpdateEndOfWeaponInFPArea()
{
	if (EndOfWeaponInFPAreaComp && EndOfWeaponComp)
	{
		if (!IsClipping())
		{
			LastEndOfWeaponInFPAreaLocation = EndOfWeaponComp->GetComponentLocation();
			EndOfWeaponInFPAreaComp->SetWorldLocation(LastEndOfWeaponInFPAreaLocation);
		}
		else
		{
			FVector EndOfWeaponLocation = EndOfWeaponComp->GetComponentLocation();

			if (fabsf(LastEndOfWeaponInFPAreaLocation.X - EndOfWeaponLocation.X) >= CurrencyToWeaponLocation)
			{
				if (LastEndOfWeaponInFPAreaLocation.Y < EndOfWeaponLocation.Y)
				{
					EndOfWeaponLocation.Y = EndOfWeaponLocation.Y - CurrencyToWeaponLocation * ChangingWeaponLocationScale;
				}
				else
				{
					EndOfWeaponLocation.Y = EndOfWeaponLocation.Y + CurrencyToWeaponLocation * ChangingWeaponLocationScale;
				}

				EndOfWeaponInFPAreaComp->SetWorldLocation(EndOfWeaponLocation);
			}
		}

		LastEndOfWeaponInFPAreaRotation = EndOfWeaponComp->GetComponentRotation();
		EndOfWeaponInFPAreaComp->SetWorldRotation(LastEndOfWeaponInFPAreaRotation);
	}
}

void UWeaponClippingComponent::UpdateAbilityToFire(float DeltaTime)
{
	if (!IsClipping() && DoOnceAfterPutDownTheWeapon)
	{
		if (CurTimeOfAbilityToFireAfterClipping < TimeOfAbilityToFireAfterClipping)
		{
			CurTimeOfAbilityToFireAfterClipping += DeltaTime;
		}
		else
		{
			OnPutDownWeapon();
		}
	}
	else
	{
		if(!FMath::IsNearlyZero(CurTimeOfAbilityToFireAfterClipping))
			CurTimeOfAbilityToFireAfterClipping = 0;
	}
}

