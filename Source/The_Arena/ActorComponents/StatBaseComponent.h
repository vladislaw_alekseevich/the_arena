// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "The_Arena/UObjects/DefaultDelegates.h"

#include "StatBaseComponent.generated.h"


USTRUCT(BlueprintType)
struct FStatBonusEffect
{
	GENERATED_USTRUCT_BODY()

	FName Name;
	float Value;
};

UCLASS()
class THE_ARENA_API UStatBaseComponent : public UActorComponent
{
	GENERATED_BODY()

	//Methods
protected:
	UStatBaseComponent();
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void ChangeStat();

	UFUNCTION()
		virtual void OnPreChangeCurrentStat();
	UFUNCTION()
		virtual void OnChangedCurrentStat();

public:
	UFUNCTION(BlueprintCallable)
		float GetMaxStat() const;
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
		void SetMaxStat(float InMaxStat);
	UFUNCTION(BlueprintCallable)
		float GetCurrentStat() const;
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
		void SetCurrentStat(float InCurrentStat);

	FDMD& GetChangedCurrentStatDispatcher();
	FDMD& GetPreChangeCurrentStatDispatcher();

	void AddBonusEffect(FName Name, float Value);
	FStatBonusEffect* FindBonusEffect(FName Name);
	void RemoveBonusEffect(FName Name);

	void AddRestrictionOnUsePositiveEffects(FName Name);
	bool FindRestrictionOnUsePositiveEffects(FName Name);
	void RemoveRestrictionOnUsePositiveEffects(FName Name);

	void AddRestrictionOnUseNegativeEffects(FName Name);
	bool FindRestrictionOnUseNegativeEffects(FName Name);
	void RemoveRestrictionOnUseNegativeEffects(FName Name);

	//Fields
protected:
	UPROPERTY(EditDefaultsOnly, Replicated, Category = "StatParameters")
		float MaxStat = 100;
	UPROPERTY(EditDefaultsOnly, Category = "StatParameters")
		float StatIncreaseSpeed = 1.0f;
	UPROPERTY()
		float Rate = 0.1f;

	UPROPERTY(ReplicatedUsing = OnChangedCurrentStat)
		float CurrentStat;

	FTimerHandle ChangeStatHandle;

	TArray<FStatBonusEffect> BonusEffects;

	TArray<FName> RestrictionsOnUsePositiveEffects;
	TArray<FName> RestrictionsOnUseNegativeEffects;

	//Events
	UPROPERTY(BlueprintAssignable)
		FDMD PreChangeCurrentStatDispatcher;
	UPROPERTY(BlueprintAssignable)
		FDMD ChangedCurrentStatDispatcher;
};
