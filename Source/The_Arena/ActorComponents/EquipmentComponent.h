// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "The_Arena/Actors/InventoryItem.h"
#include "The_Arena/ActorComponents/InventoryComponent.h"
#include "The_Arena/UObjects/DefaultDelegates.h"
#include "The_Arena/UObjects/ProjectTypes/CommonEnums.h"

#include "EquipmentComponent.generated.h"


USTRUCT(BlueprintType)
struct FEquipment
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite)
		AInventoryItemObject* InventoryItemObject;
	UPROPERTY(BlueprintReadWrite)
		AInventoryItem* InventoryItem;
};

class AFirstPersonCharacter;


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class THE_ARENA_API UEquipmentComponent : public UInventoryBaseComponent
{
	GENERATED_BODY()
	
		//Methods
public:
	// Sets default values for this component's properties
	UEquipmentComponent();

	UFUNCTION(BlueprintCallable, BlueprintPure)
		AInventoryItem* GetUsingItemInHands() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
		AInventoryItemObject* GetUsingItemObjectInHands() const;
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
		AInventoryItem* GetBackgroundUsingItem() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
		AInventoryItem* GetAdditiveUsingItem() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		FEquipment GetEquipment(EEquipmentType EquipmentType) const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
		EEquipmentType FindEquipmentType(AInventoryItemObject* InventoryItemObject);

	void RemoveItemFromBaseInventory(AInventoryItemObject* InventoryItemObject) override;	
	void DragItem(AInventoryItemObject* InventoryItemObject, UDragAndDropManagerComponent* DragAndDropComponent) override;
	void DropItemAt(int TileIndex, UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount) override;

	
	FEquipment CreateEquipment(AInventoryItemObject* InventoryItemObject, EEquipmentType EquipmentType, bool WithSaveUsingItem);
	
	FEquipment CreateOutfit(AInventoryItemObject* InventoryItemObject, bool WithSaveUsingItem);
	FEquipment CreateAdditionalEquipment(AInventoryItemObject* InventoryItemObject, bool WithSaveUsingItem);
	FEquipment CreateFirstFiringWeapon(AInventoryItemObject* InventoryItemObject, bool WithSaveUsingItem);
	FEquipment CreateSecondFiringWeapon(AInventoryItemObject* InventoryItemObject, bool WithSaveUsingItem);
	FEquipment CreatePistol(AInventoryItemObject* InventoryItemObject, bool WithSaveUsingItem);
	FEquipment CreateMelee(AInventoryItemObject* InventoryItemObject, bool WithSaveUsingItem);

	
	void RemoveEquipment(EEquipmentType EquipmentType, ERemoveItemOption RemoveItemOption);
	void RemoveEquipmentWithAddedToInventory(EEquipmentType EquipmentType);

	void RemoveOutfit(ERemoveItemOption RemoveItemOption);
	void RemoveAdditionalEquipment(ERemoveItemOption RemoveItemOption);
	void RemoveFirstFiringWeapon(ERemoveItemOption RemoveItemOption);
	void RemoveSecondFiringWeapon(ERemoveItemOption RemoveItemOption);
	void RemovePistol(ERemoveItemOption RemoveItemOption);
	void RemoveMelee(ERemoveItemOption RemoveItemOption);

	
	FDMD& GetUsingItemInHandsChangedDispatcher();	
	FDMD& GetOutfitChangedDispatcher();
	FDMD& GetAdditionalEquipmentChangedDispatcher();
	FDMD& GetFirstFiringWeaponChangedDispatcher();
	FDMD& GetSecondFiringWeaponChangedDispatcher();
	FDMD& GetPistolChangedDispatcher();
	FDMD& GetMeleeChangedDispatcher();
	FDMD& GetEndRemovingItemFromHandsDispatcher();

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ServerUseEquipment(EEquipmentType EquipmentType);

	void UseItemInHands(AInventoryItem* InventoryItem, AInventoryItemObject* InventoryItemObject);
	void RemoveItemFromHands(bool bWithDestroyItem = false);
	void UseItemOnBackground(AInventoryItem* InventoryItem);
	void UseItemAdditive(AInventoryItem* InventoryItem);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void SetUsingItemInHands(AInventoryItem* InUsingItemInHands);
	void SetUsingItemObjectInHands(AInventoryItemObject* InUsingItemObjectInHands);
	void SetBackgroundUsingItem(AInventoryItem* InBackgroundUsingItem);
	void SetAdditiveUsingItem(AInventoryItem* InAdditiveUsingItem);
	
	void SetEquipment(EEquipmentType EquipmentType, const FEquipment& InEquipment);

	FEquipment CreateEquipmentCommon(EEquipmentType EquipmentType, AInventoryItemObject* InventoryItemObject, bool WithSaveUsingItem);
	void RemoveEquipmentCommon(EEquipmentType EquipmentType, ERemoveItemOption RemoveItemOption);
	
	UFUNCTION()
		void OnRep_UsingItemInHands();
	UFUNCTION()
		void OnRep_Outfit();
	UFUNCTION()
		void OnRep_AdditionalEquipment();
	UFUNCTION()
		void OnRep_FirstFiringWeapon();
	UFUNCTION()
		void OnRep_SecondFiringWeapon();
	UFUNCTION()
		void OnRep_Pistol();
	UFUNCTION()
		void OnRep_Melee();
	UFUNCTION()
		void OnEndRemovingItemFromHands();

	//Fields
protected:
	UPROPERTY(ReplicatedUsing = OnRep_UsingItemInHands)
		AInventoryItem* UsingItemInHands;
	UPROPERTY(Replicated)
		AInventoryItemObject* UsingItemObjectInHands;
	
	UPROPERTY(Replicated)
		AInventoryItem* BackgroundUsingItem;
	UPROPERTY(Replicated)
		AInventoryItem* AdditiveUsingItem;

	UPROPERTY(ReplicatedUsing = OnRep_Outfit)
		FEquipment Outfit;
	UPROPERTY(ReplicatedUsing = OnRep_AdditionalEquipment)
		FEquipment AdditionalEquipment;
	UPROPERTY(ReplicatedUsing = OnRep_FirstFiringWeapon)
		FEquipment FirstFiringWeapon;
	UPROPERTY(ReplicatedUsing = OnRep_SecondFiringWeapon)
		FEquipment SecondFiringWeapon;
	UPROPERTY(ReplicatedUsing = OnRep_Pistol)
		FEquipment Pistol;
	UPROPERTY(ReplicatedUsing = OnRep_Melee)
		FEquipment Melee;

	UPROPERTY()
		bool bItemRemoving = false;
	UPROPERTY()
		bool bDestroyItemInHands = false;

	//Events
	UPROPERTY(BlueprintAssignable)
		FDMD UsingItemInHandsChangedDispatcher;
	
	UPROPERTY(BlueprintAssignable)
		FDMD OutfitChangedDispatcher;
	UPROPERTY(BlueprintAssignable)
		FDMD AdditionalEquipmentChangedDispatcher;
	UPROPERTY(BlueprintAssignable)
		FDMD FirstFiringWeaponChangedDispatcher;
	UPROPERTY(BlueprintAssignable)
		FDMD SecondFiringWeaponChangedDispatcher;
	UPROPERTY(BlueprintAssignable)
		FDMD PistolChangedDispatcher;
	UPROPERTY(BlueprintAssignable)
		FDMD MeleeChangedDispatcher;
	
	UPROPERTY(BlueprintAssignable)
		FDMD EndRemovingItemFromHandsDispatcher;

	//Delegates
	FD UseItemInHandsOnEndRemovingDelegate;
	FD DestroyItemInHandsDelegate;
};
