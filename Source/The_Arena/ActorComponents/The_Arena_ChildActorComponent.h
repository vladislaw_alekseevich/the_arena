// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ChildActorComponent.h"

#include "The_Arena/UObjects/DefaultDelegates.h"

#include "The_Arena_ChildActorComponent.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = Utility, hidecategories = (Object, LOD, Physics, Lighting, TextureStreaming, Activation, "Components|Activation", Collision), meta = (BlueprintSpawnableComponent))
class THE_ARENA_API UThe_Arena_ChildActorComponent : public UChildActorComponent
{
	GENERATED_BODY()

	//Methods
public:
	void CreateChildActor() override;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	AActor* GetChildActorPointer() const;

	UFUNCTION()
	void OnRep_ChildActorPointer();
	
protected:
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	//Fields
protected:
	UPROPERTY(BlueprintReadOnly, ReplicatedUsing = OnRep_ChildActorPointer)
	AActor* ChildActorPointer;

	//Events
	UPROPERTY(BlueprintAssignable)
	FDMD ChildActorPointerChangedDispatcher;
};
