// Fill out your copyright notice in the Description page of Project Settings.


#include "TransformController.h"


// Sets default values for this component's properties
UTransformController::UTransformController()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTransformController::BeginPlay()
{
	Super::BeginPlay();

	ParamsInitializtion();
}

void UTransformController::MoveParamInitialization(FSceneCompMoveParam& Param)
{
	if(Param.bUseDurationForForwardLocation)
	{
		Param.ForwardSpeedLocation = fabsf(Param.Location) / Param.ForwardDuration;
	}

	if(Param.bUseDurationForBackLocation)
	{
		Param.BackSpeedLocation = fabsf(Param.Location) / Param.BackDuration;
	}
}

void UTransformController::RotParamInitialization(FSceneCompRotParam& Param)
{
	if(Param.bUseDurationForForwardRotation)
	{
		Param.ForwardSpeedRotation = fabsf(Param.Rotation) / Param.ForwardDuration;
	}

	if(Param.bUseDurationForBackRotation)
	{
		Param.BackSpeedRotation = fabsf(Param.Rotation) / Param.BackDuration;
	}
}

void UTransformController::ParamsInitializtion()
{
	MoveParamInitialization(Forward);
	MoveParamInitialization(Right);
	MoveParamInitialization(Up);

	RotParamInitialization(Roll);
	RotParamInitialization(Pitch);
	RotParamInitialization(Yaw);
}


// Called every frame
void UTransformController::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	RotateSceneComponent(DeltaTime, Pitch, PassPitch, FRotator(1, 0, 0));
	RotateSceneComponent(DeltaTime, Yaw, PassYaw, FRotator(0, 1, 0));
	RotateSceneComponent(DeltaTime, Roll, PassRoll, FRotator(0, 0, 1));

	MoveSceneComponent(DeltaTime, Forward, PassForward, FVector(1, 0, 0));
	MoveSceneComponent(DeltaTime, Right, PassRight, FVector(0, 1, 0));
	MoveSceneComponent(DeltaTime, Up, PassUp, FVector(0, 0, 1));
}

void UTransformController::RotateSceneComponent(float DeltaTime, FSceneCompRotParam& Param, float& PassAngle, FRotator direction)
{
	auto ReturnToStartLambda = [&](int Koef) {

		float Offset = Koef * Param.BackSpeedRotation * DeltaTime * GetSpeedCoef(PassAngle, Param.BackSpeedRotationCurve);

		if (Koef * (PassAngle + Offset) > 0)
		{
			float DistanceToEndRotation = -PassAngle;

			if (SceneComp)
			{
				SceneComp->AddRelativeRotation(direction * DistanceToEndRotation);
				PassAngle = 0;
			}
		}
		else
		{
			if (SceneComp)
			{
				SceneComp->AddRelativeRotation(direction * Offset);
				PassAngle += Offset;
			}
		}
	};

	auto RotateComponentLambda = [&](int Koef, float Rotation) {
		float Offset = Koef * Param.ForwardSpeedRotation * DeltaTime * fabsf(ScaleOfMoving) * GetSpeedCoef(PassAngle, Param.ForwardSpeedRotationCurve);

		if (Koef * (PassAngle + Offset) > Koef* Rotation)
		{
			float DistanceToEndRotation = Rotation - PassAngle;

			if (SceneComp)
			{
				SceneComp->AddRelativeRotation(direction * DistanceToEndRotation);
				PassAngle = Rotation;
			}
		}
		else
		{
			if (SceneComp)
			{
				SceneComp->AddRelativeRotation(direction * Offset);
				PassAngle += Offset;
			}
		}
	};

	if (FMath::IsNearlyZero(ScaleOfMoving, CurrencyScaleOfMoving))
	{
		if (PassAngle < 0)
		{
			ReturnToStartLambda(1);
		}
		else if (PassAngle > 0)
		{
			ReturnToStartLambda(-1);
		}
	}
	else if (ScaleOfMoving > 0)
	{
		if (PassAngle < Param.Rotation)
		{
			RotateComponentLambda(1, Param.Rotation);
		}
		else if (PassAngle > Param.Rotation)
		{
			RotateComponentLambda(-1, Param.Rotation);
		}
	}
	else
	{
		if (Param.Mirror)
		{
			if (PassAngle < -Param.Rotation)
			{
				RotateComponentLambda(1, -Param.Rotation);
			}
			else if (PassAngle > -Param.Rotation)
			{
				RotateComponentLambda(-1, -Param.Rotation);
			}
		}
		else
		{
			if (PassAngle < Param.Rotation)
			{
				RotateComponentLambda(1, Param.Rotation);
			}
			else if (PassAngle > Param.Rotation)
			{
				RotateComponentLambda(-1, Param.Rotation);
			}
		}
	}
}

void UTransformController::MoveSceneComponent(float DeltaTime, FSceneCompMoveParam& Param, float& PassDistance, FVector direction)
{
	auto ReturnToStartLambda = [&](int Koef) {

		float Offset = Koef * Param.BackSpeedLocation * DeltaTime * GetSpeedCoef(PassDistance, Param.BackSpeedLocationCurve);

		if (Koef * (PassDistance + Offset) > 0)
		{
			float DistanceToEndLocation = -PassDistance;

			if (SceneComp)
			{
				SceneComp->AddRelativeLocation(direction * DistanceToEndLocation);
				PassDistance = 0;
			}
		}
		else
		{
			if (SceneComp)
			{
				SceneComp->AddRelativeLocation(direction * Offset);
				PassDistance += Offset;
			}
		}
	};

	auto MoveComponentLambda = [&](int Koef, float Location) {
		float Offset = Koef * Param.ForwardSpeedLocation * DeltaTime * fabsf(ScaleOfMoving) * GetSpeedCoef(PassDistance, Param.ForwardSpeedLocationCurve);

		if (Koef * (PassDistance + Offset) > Koef* Location)
		{
			float DistanceToEndLocation = Location - PassDistance;

			if (SceneComp)
			{
				SceneComp->AddRelativeLocation(direction * DistanceToEndLocation);
				PassDistance = Location;
			}
		}
		else
		{
			if (SceneComp)
			{
				SceneComp->AddRelativeLocation(direction * Offset);
				PassDistance += Offset;
			}
		}
	};

	if (FMath::IsNearlyZero(ScaleOfMoving, CurrencyScaleOfMoving))
	{
		if (PassDistance < 0)
		{
			ReturnToStartLambda(1);
		}
		else if (PassDistance > 0)
		{
			ReturnToStartLambda(-1);
		}
	}
	else if (ScaleOfMoving > 0)
	{
		if (PassDistance < Param.Location)
		{
			MoveComponentLambda(1, Param.Location);
		}
		else if (PassDistance > Param.Location)
		{
			MoveComponentLambda(-1, Param.Location);
		}
	}
	else
	{
		if (Param.Mirror)
		{
			if (PassDistance < -Param.Location)
			{
				MoveComponentLambda(1, -Param.Location);
			}
			else if (PassDistance > -Param.Location)
			{
				MoveComponentLambda(-1, -Param.Location);
			}
		}
		else
		{
			if (PassDistance < Param.Location)
			{
				MoveComponentLambda(1, Param.Location);
			}
			else if (PassDistance > Param.Location)
			{
				MoveComponentLambda(-1, Param.Location);
			}
		}
	}
}

void UTransformController::SetScaleOfMoving(float Scale)
{
	ScaleOfMoving = Scale;
}

float UTransformController::GetScaleOfMoving() const
{
	return ScaleOfMoving;
}

float UTransformController::GetSpeedCoef(float InTime, UCurveFloat* Curve) const
{
	if (Curve == nullptr)
	{
		return 1.0f;
	}

	return Curve->GetFloatValue(InTime);
}

void UTransformController::SetSceneComp(USceneComponent* Component)
{
	SceneComp = Component;
}

USceneComponent* UTransformController::GetSceneComp() const
{
	return SceneComp;
}

void UTransformController::SetPassLocation(FVector Location)
{
	PassForward = Location.X;
	PassRight = Location.Y;
	PassUp = Location.Z;
}

void UTransformController::SetPassRotation(FRotator Rotation)
{
	PassRoll = Rotation.Roll;
	PassPitch = Rotation.Pitch;
	PassYaw = Rotation.Yaw;
}

FVector UTransformController::GetPassLocation() const
{
	return FVector(PassForward, PassRight, PassUp);
}

FRotator UTransformController::GetPassRotation() const
{
	return FRotator(PassPitch, PassYaw, PassRoll);
}
