// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "The_Arena/ActorComponents/StatBaseComponent.h"

#include "HealthComponent.generated.h"

/*
 * This class will big. Don't move it to FPCharacter.
 */
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THE_ARENA_API UHealthComponent : public UStatBaseComponent
{
	GENERATED_BODY()

	//Methods
protected:
	UHealthComponent();
	void BeginPlay() override;
	void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
public:
	UFUNCTION(BlueprintCallable)
	bool IsAlive() const;
};
