// Fill out your copyright notice in the Description page of Project Settings.


#include "FPSoundComponent.h"
#include "The_Arena/Characters/FirstPersonCharacter.h"


// Sets default values for this component's properties
UFPSoundComponent::UFPSoundComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UFPSoundComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...	
	FPOwner = Cast<AFirstPersonCharacter>(GetOwner());
}


// Called every frame
void UFPSoundComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UpdateStepSound(DeltaTime);
}

void UFPSoundComponent::PlayStepSound()
{
	if (FPOwner)
	{
		if (!FPOwner->GetCharacterMovement()->IsFalling())
		{
			if (SoundAsset)
			{
				PlaySound(SoundAsset->StepSounds, CurStepNumberOfSound);
			}
		}
	}
}

void UFPSoundComponent::UpdateStepSound(float DeltaTime)
{
	if (!SoundAsset || !FPOwner)
		return;

	auto MovementComp = FPOwner->GetMovementComponent();
	if (FPOwner->GetCurrentVelocity() <= BlockingStepSoundSpeed || MovementComp->IsFalling())
	{
		if(!FMath::IsNearlyZero(CurrentStepSoundFrequency, 0.1f))
		{
			PlayStepSound();
		}
		
		CurrentStepSoundFrequency = 0;
	}
	else
	{
		CurrentStepSoundFrequency += DeltaTime;

		if ((CurrentWalkingMode == EMovementType::FastWalk && CurrentStepSoundFrequency >= SoundAsset->WalkSoundFrequency) ||
			(CurrentWalkingMode == EMovementType::Run && CurrentStepSoundFrequency >= SoundAsset->RunSoundFrequency) ||
			(CurrentWalkingMode == EMovementType::SlowWalk && CurrentStepSoundFrequency >= SoundAsset->SlowWalkSoundFrequency) ||
			(CurrentWalkingMode == EMovementType::CrouchWalk && CurrentStepSoundFrequency >= SoundAsset->CrouchSoundFrequency))
		{
			PlayStepSound();
			CurrentStepSoundFrequency = 0;
		}
	}
}

bool UFPSoundComponent::IsGroundUnderCharacterExist(FHitResult& OutResult)
{
	FVector Start = FPOwner->GetActorLocation();
	FVector End = FPOwner->GetActorLocation() + FPOwner->GetActorUpVector() * (-1000);
	FCollisionQueryParams QueryParams;
	QueryParams.AddIgnoredActor(FPOwner);
	QueryParams.bReturnPhysicalMaterial = true;

	if (GetWorld()->LineTraceSingleByChannel(OutResult, Start, End, ECC_Visibility, QueryParams))
	{
		return true;
	}

	return false;
}

void UFPSoundComponent::CalculateRandomNumberOfSound(int SoundArraySize, int& CurNumberOfSound)
{
	int NumberOfSound = FMath::RandRange(0, SoundArraySize - 1);

	if (NumberOfSound == CurNumberOfSound)
	{
		NumberOfSound++;

		if (NumberOfSound >= SoundArraySize)
		{
			NumberOfSound = 0;
		}
	}

	CurNumberOfSound = NumberOfSound;
}

void UFPSoundComponent::PlaySound(TArray<FFootstepSoundParam>& Sounds, int& CurNumberOfSound)
{
	FHitResult OutResult;

	if (IsGroundUnderCharacterExist(OutResult))
	{
		auto SurfaceType = UGameplayStatics::GetSurfaceType(OutResult);

		for (int i = 0; i < Sounds.Num(); i++)
		{
			if (Sounds[i].Surface == SurfaceType)
			{
				if (Sounds[i].Sounds.Num() == 0)
					return;

				CalculateRandomNumberOfSound(Sounds[i].Sounds.Num(), CurNumberOfSound);

				if (FPOwner)
				{
					if (FPOwner->GetLocalRole() == ROLE_Authority)
					{
						MulticastSpawnSound(Sounds[i].Sounds[CurNumberOfSound]);
					}

					if (FPOwner->IsLocallyControlled())
					{
						SpawnSound(Sounds[i].Sounds[CurNumberOfSound]);
					}
				}

				break;
			}
		}
	}
}

void UFPSoundComponent::SpawnSound(USoundBase* Sound)
{
	if (!Sound || !FPOwner)
		return;

	float CrouchVolumeMultiplier = 1.f;
	float RemoteVolumeMultiplier = 1.f;
	USoundAttenuation* SoundAttenuationLocal = nullptr;
	
	if(FPOwner->bIsCrouched)
	{
		CrouchVolumeMultiplier = SoundAsset->CrouchVolumeMultiplier;
		SoundAttenuationLocal = QuietCharacter3DAttenuation;
	}
	else
	{
		SoundAttenuationLocal = LoudCharacter3DAttenuation;
	}

	if(FPOwner->IsLocallyControlled())
	{
		SoundAttenuationLocal = Character2DAttenuation;
	}
	else
	{
		RemoteVolumeMultiplier = SoundAsset->RemoteVolumeMultiplier;
	}
	
	UGameplayStatics::SpawnSoundAttached(Sound, FPOwner->GetRootComponent(), NAME_None, FVector(ForceInit), FRotator::ZeroRotator, EAttachLocation::KeepRelativeOffset, false, CrouchVolumeMultiplier * RemoteVolumeMultiplier, 1, 0, SoundAttenuationLocal);
}

void UFPSoundComponent::MulticastSpawnSound_Implementation(USoundBase* Sound)
{
	if (FPOwner && !FPOwner->IsLocallyControlled())
	{
		SpawnSound(Sound);
	}
}

void UFPSoundComponent::PlayJumpSound()
{
	if (SoundAsset)
	{
		PlaySound(SoundAsset->JumpSounds, CurJumpNumberOfSound);
	}
}

void UFPSoundComponent::ClientPlayDamageSound_Implementation()
{
	if (!DamageSound)
		return;

	UGameplayStatics::PlaySound2D(this, DamageSound);
}

void UFPSoundComponent::PlayDeathSound()
{
	if (!DeathSound)
		return;
	
	if(FPOwner)
	{
		if(auto CharacterVoiceComponent = FPOwner->GetCharacterVoiceComponent())
		{
			CharacterVoiceComponent->SetSound(DeathSound);
			CharacterVoiceComponent->AttenuationSettings = LoudCharacter3DAttenuation;
			CharacterVoiceComponent->Play();
		}
	}
}

void UFPSoundComponent::PlayCoughSound()
{
	if (!CoughSound || bCoughPlayed)
		return;

	if (FPOwner && FPOwner->IsAlive())
	{
		if (auto CharacterVoiceComponent = FPOwner->GetCharacterVoiceComponent())
		{
			if(!CharacterVoiceComponent->IsPlaying() || (CharacterVoiceComponent->IsPlaying() && CharacterVoiceComponent->Sound != CoughSound))
			{
				CharacterVoiceComponent->SetSound(CoughSound);

				if (FPOwner->IsLocallyControlled())
				{
					CharacterVoiceComponent->AttenuationSettings = Character2DAttenuation;
				}
				else
				{
					CharacterVoiceComponent->AttenuationSettings = QuietCharacter3DAttenuation;
				}

				CharacterVoiceComponent->Play();
				bCoughPlayed = true;
			}
		}
	}
}

void UFPSoundComponent::StopCoughSound()
{
	if (!CoughSound)
		return;

	if(FPOwner)
	{
		if (auto CharacterVoiceComponent = FPOwner->GetCharacterVoiceComponent())
		{
			if(CharacterVoiceComponent->Sound == CoughSound)
			{
				CharacterVoiceComponent->Stop();
				bCoughPlayed = false;
			}
		}
	}
}

void UFPSoundComponent::PlayBreathLoopSound()
{
	if (!BreathLoopSound || bCoughPlayed)
		return;

	if (FPOwner && FPOwner->IsAlive())
	{
		if (auto CharacterVoiceComponent = FPOwner->GetCharacterVoiceComponent())
		{
			if (!CharacterVoiceComponent->IsPlaying())
			{
				CharacterVoiceComponent->SetSound(BreathLoopSound);

				if (FPOwner->IsLocallyControlled())
				{
					CharacterVoiceComponent->AttenuationSettings = Character2DAttenuation;
				}
				else
				{
					CharacterVoiceComponent->AttenuationSettings = QuietCharacter3DAttenuation;
				}

				if(!CharacterVoiceComponent->OnAudioFinished.IsAlreadyBound(this, &UFPSoundComponent::PlayBreathPantSound))
				{
					CharacterVoiceComponent->OnAudioFinished.AddDynamic(this, &UFPSoundComponent::PlayBreathPantSound);
				}
				
				CharacterVoiceComponent->FadeIn(2.0f, 1.0f);
			}
		}
	}
}

void UFPSoundComponent::StopBreathLoopSound()
{
	if (!BreathLoopSound)
		return;

	if (FPOwner)
	{
		if (auto CharacterVoiceComponent = FPOwner->GetCharacterVoiceComponent())
		{
			if (CharacterVoiceComponent->Sound == BreathLoopSound)
			{
				CharacterVoiceComponent->FadeOut(2.0f, 0.0f);
			}
		}
	}
}

void UFPSoundComponent::PlayBreathPantSound()
{
	if (!BreathPantSound || bCoughPlayed)
		return;

	if (FPOwner && FPOwner->IsAlive())
	{
		if (auto CharacterVoiceComponent = FPOwner->GetCharacterVoiceComponent())
		{
			CharacterVoiceComponent->SetSound(BreathPantSound);

			if (FPOwner->IsLocallyControlled())
			{
				CharacterVoiceComponent->AttenuationSettings = Character2DAttenuation;
			}
			else
			{
				CharacterVoiceComponent->AttenuationSettings = QuietCharacter3DAttenuation;
			}

			if (CharacterVoiceComponent->OnAudioFinished.IsAlreadyBound(this, &UFPSoundComponent::PlayBreathPantSound))
			{
				CharacterVoiceComponent->OnAudioFinished.RemoveDynamic(this, &UFPSoundComponent::PlayBreathPantSound);
			}

			CharacterVoiceComponent->Play();
		}
	}
}

void UFPSoundComponent::SetCurrentWalkingMode(EMovementType Mode)
{
	if (Mode != CurrentWalkingMode)
	{
		CurrentWalkingMode = Mode;
		CurrentStepSoundFrequency = 0;
	}
}


