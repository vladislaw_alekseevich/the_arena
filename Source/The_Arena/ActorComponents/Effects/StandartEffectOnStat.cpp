// Fill out your copyright notice in the Description page of Project Settings.


#include "StandartEffectOnStat.h"

#include "The_Arena/ActorComponents/StatBaseComponent.h"


void UStandartEffectOnStat::StandartEffectBaseOnStatInitialization(float InAmountEffectPerSec, TSubclassOf<UStatBaseComponent> InStatComponentClass, FCharacteristicEffectParams InEffectParams)
{
	AmountEffectPerSec = InAmountEffectPerSec;
	
	StatEffectBaseInitialization(InStatComponentClass, InEffectParams);
}

float UStandartEffectOnStat::GetAmountEffectPerSec() const
{
	return AmountEffectPerSec;
}

void UStandartEffectOnStat::CreateEffectInternal()
{
	Super::CreateEffectInternal();
	
	CreateStandartEffect();	
}

void UStandartEffectOnStat::CreateStandartEffect()
{	
	if(StatComponent)
	{
		StatComponent->AddBonusEffect(EffectParams.EffectName, AmountEffectPerSec);
	}
}

void UStandartEffectOnStat::RemoveEffectInternal()
{
	if(StatComponent)
	{
		StatComponent->RemoveBonusEffect(EffectParams.EffectName);
	}
	
	Super::RemoveEffectInternal();
}
