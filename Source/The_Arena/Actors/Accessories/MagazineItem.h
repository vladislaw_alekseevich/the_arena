// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "The_Arena/Actors/InventoryItem.h"
#include "MagazineItem.generated.h"

/**
 * 
 */
UCLASS()
class THE_ARENA_API AMagazineItem : public AInventoryItem
{
	GENERATED_BODY()

	//Methods
protected:
	virtual void InitializationOnWeapon() override;
	virtual void DeinitializationFromWeapon() override;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		int GetMaxBulletsInMagazine() const;
	
	void MagazineInitialization();
	void MagazineDeInitialization();

	//Fields
protected:
	UPROPERTY(EditDefaultsOnly)
		int MaxBulletsInMagazine;
};
