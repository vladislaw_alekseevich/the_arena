// Fill out your copyright notice in the Description page of Project Settings.


#include "ScopeItem.h"
#include "The_Arena/Actors/Weapons/RifleBase.h"


void AScopeItem::InitializationOnWeapon()
{
	Super::InitializationOnWeapon();

	ScopeInitialization();
}

void AScopeItem::DeinitializationFromWeapon()
{
	Super::DeinitializationFromWeapon();

	ScopeDeInitialization();
}

void AScopeItem::ScopeInitialization()
{
	if (auto FiringWeapon = Cast<ARifleBase>(GetOwner()))
	{		
		FiringWeapon->GetAimingDispatcher().AddDynamic(this, &AScopeItem::Aim);

		if(auto FPCharacter = Cast<AFirstPersonCharacter>(FiringWeapon->GetOwner()))
		{
			if(auto FPSkeletalMesh = FPCharacter->GetFirstPersonSkeletalMesh())
			{
				if (ScopeTransformController)
				{
					ScopeTransformController->SetSceneComp(FPSkeletalMesh);

					if (ScopeInitializationTimer.IsValid())
					{
						GetWorldTimerManager().ClearTimer(ScopeInitializationTimer);
					}
					
					return;
				}
			}
		}
	}

	if(!ScopeInitializationTimer.IsValid())
	{
		GetWorldTimerManager().SetTimer(ScopeInitializationTimer, this, &AScopeItem::ScopeInitialization, 0.1f, true);
	}
}

void AScopeItem::ScopeDeInitialization()
{
	if (auto FiringWeapon = Cast<ARifleBase>(GetOwner()))
	{		
		FiringWeapon->GetAimingDispatcher().RemoveDynamic(this, &AScopeItem::Aim);
	}
}

void AScopeItem::Aim(bool IsAiming)
{
	auto ReturnAimingFOVLambda = [&]()
	{
		if (auto FiringWeapon = Cast<ARifleBase>(GetOwner()))
		{
			if(bAdditionalScopeFOVAdded)
			{
				FiringWeapon->SetAimingFOV(FiringWeapon->GetAimingFOV() - AdditionalScopeFOV);
				bAdditionalScopeFOVAdded = false;
			}		
		}
	};
	
	if(ScopeTransformController)
	{
		if(IsAiming)
		{
			ScopeTransformController->SetScaleOfMoving(1.0f);

			if (auto FiringWeapon = Cast<ARifleBase>(GetOwner()))
			{
				if(!bAdditionalScopeFOVAdded)
				{
					FiringWeapon->SetAimingFOV(FiringWeapon->GetAimingFOV() + AdditionalScopeFOV);
					bAdditionalScopeFOVAdded = true;
				}

				GetWorldTimerManager().ClearTimer(ReturnAimingFOVHandle);
			}
		}
		else
		{
			ScopeTransformController->SetScaleOfMoving(0.0f);

			if (auto FiringWeapon = Cast<ARifleBase>(GetOwner()))
			{
				GetWorldTimerManager().SetTimer(ReturnAimingFOVHandle, ReturnAimingFOVLambda, FiringWeapon->GetTimeFOVLerp(), false);
			}	
		}
	}
}
