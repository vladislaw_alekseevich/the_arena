// Fill out your copyright notice in the Description page of Project Settings.


#include "NPC.h"

#include "The_Arena/ActorComponents/CustomInventoryComponent.h"
#include "The_Arena/ActorComponents/TraderInventoryComponent.h"


// Sets default values
ANPC::ANPC()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ANPC::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ANPC::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ANPC::Interact_Implementation(AActor* InteractionCauser)
{
	if (!InteractionCauser || GetLocalRole() != ROLE_Authority)
		return;

	auto PlayerCustomInventoryComponent = InteractionCauser->FindComponentByClass<UCustomInventoryComponent>();
	auto TraderInventoryComponent = FindComponentByClass<UTraderInventoryComponent>();

	if (PlayerCustomInventoryComponent && TraderInventoryComponent)
	{
		PlayerCustomInventoryComponent->GetInventoryOpened_PC_Dispatcher().AddDynamic(TraderInventoryComponent, &UTraderInventoryComponent::TraderInventoryOpeningInitialization);
		PlayerCustomInventoryComponent->GetInventoryClosed_PC_Dispatcher().AddDynamic(TraderInventoryComponent, &UTraderInventoryComponent::TraderInventoryClosingInitialization);
	}
}

FString ANPC::GetInteractionText_Implementation()
{
	return "Trade";
}

bool ANPC::CanInteracted_Implementation() const
{
	return true;
}

