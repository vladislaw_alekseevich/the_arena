// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "The_Arena/Actors/InventoryItem.h"
#include "The_Arena/Interfaces/InHandsUsable.h"
#include "The_Arena/UObjects/DefaultDelegates.h"
#include "The_Arena/UObjects/DataAssets/JumpPriorityDataAsset.h"
#include "The_Arena/UObjects/DataAssets/PriorityWithOneEqualParamAsset.h"
#include "The_Arena/ActorComponents/FPPlayerControllerRepParams.h"
#include "Animation/AnimInstance.h"

#include "Weapon.generated.h"

USTRUCT(BlueprintType)
struct FAblePriority
{
	GENERATED_USTRUCT_BODY()

	TEnumAsByte<EAbility> Type;
	int Priority;
	FString Name;
};

UENUM(BlueprintType)
enum EWeaponAction
{
	Draw, Fire, Aiming, Reload, ChangeFireMode, Holster
};


UCLASS()
class THE_ARENA_API AWeapon : public AInventoryItem, public IInHandsUsable
{
	GENERATED_BODY()

	//Methods
public:
	AWeapon();

	UFUNCTION(NetMulticast, Reliable)
	void MulticastOnBackInitialization(EEquipmentType EquipmentType);
	
	void Use_Implementation() override;
	void Remove_Implementation() override;
	void BindToRemovedDispatcher_Implementation(UObject* InObject, const FName& InFunctionName) override;
	void UnbindFromRemovedDispatcher_Implementation(UObject* InObject, const FName& InFunctionName) override;
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
		float GetDamage() const;
	UFUNCTION(BlueprintCallable)
		void SetDamage(float Value);

	UFUNCTION(BlueprintCallable, BlueprintPure)
		float GetAttackTime() const;

	FDMD& GetAttackDispatcher();
	FDMD_Bool& GetPureAttackDispatcher();
	FDMD& GetOnBackInitializationDispatcher();
	FDMD& GetInHandsInitializationDispatcher();
	FDMD_Bool& GetFireButtonPressedDispatcher();
	FDMD_Bool& GetDrawingDispatcher();
	FDMD_Bool& GetHolsteringDispatcher();

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsPureAttack();
	UFUNCTION(BlueprintCallable)
		void SetPureAttack(bool bInPureAttack);
	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsDrawing() const;
	UFUNCTION(BlueprintCallable)
		void SetDrawing(bool bInDrawing);
	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsHolstering() const;
	UFUNCTION(BlueprintCallable)
		void SetHolstering(bool bInHolstering);

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsInHandsInitialized() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsFireButtonPressed() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		USkeletalMeshComponent* GetFPWeaponMeshComponent() const;

	UFUNCTION(BlueprintCallable)
		virtual void AddPriority(EWeaponAction Action, FString PriorityName, EAbility Ability);
	UFUNCTION(NetMulticast, Reliable)
		void MulticastAddPriority(EWeaponAction Action, const FString& PriorityName, EAbility Ability);

	UFUNCTION(BlueprintCallable)
		virtual bool FindPriority(EWeaponAction Action, FString PriorityName, EAbility Ability);

	UFUNCTION(BlueprintCallable)
		virtual void RemovePriority(EWeaponAction Action, FString PriorityName, EAbility Ability);
	UFUNCTION(NetMulticast, Reliable)
		void MulticastRemovePriority(EWeaponAction Action, const FString& PriorityName, EAbility Ability);

	UFUNCTION(BlueprintCallable, BlueprintPure)
		virtual FAblePriority GetMinPriority(EWeaponAction Action) const;

protected:
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
	UFUNCTION()
	virtual void InHandsInitialization();
	UFUNCTION()
	virtual	void InHandsDeInitialization();
	UFUNCTION(NetMulticast, Reliable)
	void MulticastInHandsDeinitialization();

	UFUNCTION()
	virtual void OnBackInitialization(EEquipmentType EquipmentType);

	virtual void OnFireButtonPressed_Implementation() override;
	UFUNCTION(Server, Reliable)
		void ServerOnFireButtonPressed();
	UFUNCTION(NetMulticast, Reliable)
		void MulticastOnFireButtonPressed();

	virtual void OnFireButtonReleased_Implementation() override;
	UFUNCTION(Server, Reliable)
		void ServerOnFireButtonReleased();
	UFUNCTION(NetMulticast, Reliable)
		void MulticastOnFireButtonReleased();

	virtual void OnAimingButtonPressed_Implementation() override;
	UFUNCTION(Server, Reliable)
		void ServerOnAimingButtonPressed();
	UFUNCTION(NetMulticast, Reliable)
		void MulticastOnAimingButtonPressed();

	virtual void OnAimingButtonReleased_Implementation() override;
	UFUNCTION(Server, Reliable)
		void ServerOnAimingButtonReleased();
	UFUNCTION(NetMulticast, Reliable)
		void MulticastOnAimingButtonReleased();

	virtual void OnReloadButtonPressed_Implementation() override;
	UFUNCTION(Server, Reliable)
		void ServerOnReloadButtonPressed();
	UFUNCTION(NetMulticast, Reliable)
		void MulticastOnReloadButtonPressed();
	
	UFUNCTION()
	virtual void OnDrawing(bool bInDrawing);
	UFUNCTION()
	virtual void OnHolstering(bool bInHolstering);
	
	UFUNCTION()
		virtual void OnAttack();
	UFUNCTION()
		void OnPureAttack(bool bInPureAttack);

	UFUNCTION()
		virtual void ApplyDamage(FHitResult Result);
	UFUNCTION(Server, Reliable)
		void ServerApplyDamage(FHitResult Result);

	UFUNCTION()
		virtual void AfterDrawing();
	UFUNCTION()
		virtual void AfterHolstering();
	UFUNCTION(NetMulticast, Reliable)
		void MulticastAfterHolstering();

	UFUNCTION()
		virtual void AddDefaultPriorities();

	UFUNCTION()
		void AddPriorityProtected(FAblePriority Priority, TArray<FAblePriority>& Array);
	UFUNCTION()
		bool FindPriorityProtected(FAblePriority Priority, TArray<FAblePriority>& Array);
	UFUNCTION()
		void RemovePriorityProtected(FAblePriority Priority, TArray<FAblePriority>& Array);

	UFUNCTION()
		FAblePriority GetMinPriorityProtected(const TArray<FAblePriority>& Array) const;
	
	//Fields
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		USkeletalMeshComponent* FPWeaponSkeletalMesh;

	UPROPERTY(EditDefaultsOnly, Category = "FirstPersonView")
		TSubclassOf<UAnimInstance> FPCharacterAnimBP;
	UPROPERTY(EditDefaultsOnly, Category = "FirstPersonView")
		FVector RelativeWeaponHandsLocation;
	UPROPERTY(EditDefaultsOnly, Category = "FirstPersonView")
		FRotator RelativeWeaponHandsRotation;

	UPROPERTY(EditDefaultsOnly, Category = "ThirdPersonView")
		TSubclassOf<UAnimInstance> TPCharacterAnimBP;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
		TSubclassOf<UUserWidget> WeaponAimWidget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Damage")
		TSubclassOf<UDamageType> DamageType;
	
	UPROPERTY(EditDefaultsOnly, Category = "Damage")
		float Damage = 30.0f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
		USoundBase* DefaultHitSound;

	UPROPERTY(EditDefaultsOnly, Category = "DurationActions")
		float DrawTime = 0.0f;
	UPROPERTY(EditDefaultsOnly, Category = "DurationActions")
		float PureAttackTime = 0.3f; //Shot without delay
	UPROPERTY(EditDefaultsOnly, Category = "DurationActions")
		float AttackTime = 0.3f; // Shot + delay
	UPROPERTY(EditDefaultsOnly, Category = "DurationActions")
		float HolsterTime = 0.0f;

	UPROPERTY(EditDefaultsOnly, Category = "WeaponPriorities")
		UPriorityWithOneEqualParamAsset* FirePrioritiesAsset;
	UPROPERTY(EditDefaultsOnly, Category = "WeaponPriorities")
		UPriorityWithOneEqualParamAsset* HolsterPrioritiesAsset;

	UPROPERTY()
		EEquipmentType WeaponEquipmentType;
	UPROPERTY()
		UFPPlayerControllerRepParams* p_FPPlayerControllerRepParams;

	TArray<FAblePriority> FirePriorities;
	TArray<FAblePriority> HolsterPriorities;

	bool bDrawing = true;
	bool bHolstering = false;	
	bool bFireButtonPressed = false;
	bool bAimingButtonPressed = false;
	bool bInHandsInitialized = false;
	bool bPureAttack = false;

	FTimerHandle PureAttackHandle;
	FTimerHandle DrawHandle;
	FTimerHandle HolsterHandle;
	FTimerHandle InitializaitonHandle;

	//Events
	UPROPERTY(BlueprintAssignable)
	FDMD BackInitializationDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD InHandsInitializationDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD_Bool FireButtonPressedDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD_Bool DrawingDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD AttackDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD_Bool PureAttackDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD_Bool HolsteringDispatcher;
};
