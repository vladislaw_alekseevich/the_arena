// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponMountBase.h"
#include "Net/UnrealNetwork.h"

#include "The_Arena/Actors/Weapons/RifleBase.h"


void AWeaponMountBase::MountInitialization(UMotherConnectorComponent* InMotherConnector, UMeshComponent* InActivatedWeaponModuleMesh, UMeshComponent* InDeactivatedWeaponModuleMesh)
{
	if (InMotherConnector && GetLocalRole() == ROLE_Authority)
	{
		InMotherConnector->GetSetupAttachmentDispatcher().AddDynamic(this, &AWeaponMountBase::ActivateMount);
		InMotherConnector->GetRemoveAttachmentDispatcher().AddDynamic(this, &AWeaponMountBase::DeactivateMount);
	}

	ActivatedWeaponModuleMesh = InActivatedWeaponModuleMesh;
	DeactivatedWeaponModuleMesh = InDeactivatedWeaponModuleMesh;

	bMountInitialized = true;
}

void AWeaponMountBase::ActivateMount()
{
	if (GetLocalRole() == ROLE_Authority)
	{
		bMountActivated = true;
		OnRep_MountActivated();
	}
}

void AWeaponMountBase::DeactivateMount()
{
	if (GetLocalRole() == ROLE_Authority)
	{
		bMountActivated = false;
		OnRep_MountActivated();
	}
}

bool AWeaponMountBase::IsActivated() const
{
	return bMountActivated;
}

bool AWeaponMountBase::IsMountInitialized() const
{
	return bMountInitialized;
}

// Sets default values
AWeaponMountBase::AWeaponMountBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWeaponMountBase::BeginPlay()
{
	Super::BeginPlay();

	if (GetLocalRole() == ROLE_Authority)
	{
		if (auto Parent = GetParentActor())
		{
			SetOwner(Parent);
			OnRep_Owner();
		}
	}
}

// Called every frame
void AWeaponMountBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWeaponMountBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AWeaponMountBase, bMountActivated);
}

void AWeaponMountBase::UpdateMountActivatedVisibility()
{
	if (MountStaticMesh)
	{
		MountStaticMesh->SetVisibility(true);
	}

	if (ActivatedWeaponModuleMesh)
	{
		ActivatedWeaponModuleMesh->SetVisibility(true);
	}

	if (DeactivatedWeaponModuleMesh)
	{
		DeactivatedWeaponModuleMesh->SetVisibility(false);
	}
}

void AWeaponMountBase::UpdateMountDeactivatedVisibility()
{
	if (MountStaticMesh)
	{
		MountStaticMesh->SetVisibility(false);
	}

	if (ActivatedWeaponModuleMesh)
	{
		ActivatedWeaponModuleMesh->SetVisibility(false);
	}

	if (DeactivatedWeaponModuleMesh)
	{
		DeactivatedWeaponModuleMesh->SetVisibility(true);
	}
}

void AWeaponMountBase::UpdateMountHideVisibility()
{
	if (MountStaticMesh)
	{
		MountStaticMesh->SetVisibility(false);
	}

	if (ActivatedWeaponModuleMesh)
	{
		ActivatedWeaponModuleMesh->SetVisibility(false);
	}

	if (DeactivatedWeaponModuleMesh)
	{
		DeactivatedWeaponModuleMesh->SetVisibility(false);
	}
}

void AWeaponMountBase::OnRep_MountActivated()
{
	if(bMountActivated)
	{
		++AttachedModules;
	}
	else
	{
		--AttachedModules;
	}
}
