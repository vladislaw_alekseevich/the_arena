// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryItem.h"
#include "Net/UnrealNetwork.h"

#include "The_Arena/GameStates/CustomGameState.h"
#include "The_Arena/UObjects/DataAssets/InventoryItemCommonParams.h"
#include "The_Arena/UObjects/Factories/InventoryItemsManager.h"
#include "The_Arena/Actors/Weapons/RifleBase.h"


// Sets default values
AInventoryItem::AInventoryItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	NetUpdateFrequency = 20.0f;

}

void AInventoryItem::Initialization(bool bInTestItem)
{
	bTestItem = bInTestItem;

	ModuleInitialization();
	CreateTPInventoryItem();
}

void AInventoryItem::MulticastInitialization_Implementation(bool bInTestItem)
{
	Initialization(bInTestItem);
}

UInventoryItemCommonParams* AInventoryItem::GetInventoryItemCommonParams() const
{
	return InventoryItemCommonParams;
}

UMeshComponent* AInventoryItem::GetItemMeshComponent() const
{
	return ItemMeshComponent;
}

TSubclassOf<ATPInventoryItem> AInventoryItem::GetTPInventoryItemClass() const
{
	return TPInventoryItemClass;
}

ATPInventoryItem* AInventoryItem::GetTPInventoryItem() const
{
	return TPInventoryItem;
}

bool AInventoryItem::IsTestItem() const
{
	return bTestItem;
}

// Called when the game starts or when spawned
void AInventoryItem::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AInventoryItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AInventoryItem::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(AInventoryItem, TPInventoryItem);
	DOREPLIFETIME(AInventoryItem, ItemMeshComponent);
}

void AInventoryItem::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	ModuleDeinitialization();
	
	if(TPInventoryItem)
	{
		TPInventoryItem->Destroy();
	}

	Super::EndPlay(EndPlayReason);
}

void AInventoryItem::ModuleInitialization()
{
	InitializationOnWeapon();
}

void AInventoryItem::ModuleDeinitialization()
{
	DeinitializationFromWeapon();
}

void AInventoryItem::InitializationOnWeapon()
{
	if (auto FiringWeapon = Cast<ARifleBase>(GetOwner()))
	{
		FiringWeapon->GetInHandsInitializationDispatcher().AddDynamic(this, &AInventoryItem::ShowItemMesh);
		FiringWeapon->GetOnBackInitializationDispatcher().AddDynamic(this, &AInventoryItem::HideItemMesh);

		if(auto FPWeaponMesh = FiringWeapon->GetFPWeaponMeshComponent())
		{
			if(!FPWeaponMesh->IsVisible())
			{
				ItemMeshComponent->SetVisibility(false);
			}
		}
	}
}

void AInventoryItem::DeinitializationFromWeapon()
{
	if (auto FiringWeapon = Cast<ARifleBase>(GetOwner()))
	{
		FiringWeapon->GetInHandsInitializationDispatcher().RemoveDynamic(this, &AInventoryItem::ShowItemMesh);
		FiringWeapon->GetOnBackInitializationDispatcher().RemoveDynamic(this, &AInventoryItem::HideItemMesh);
	}
}

void AInventoryItem::CreateTPInventoryItem()
{
	if(GetLocalRole() == ROLE_Authority)
	{
		FActorSpawnParameters Parameters;
		Parameters.Owner = this;
		
		TPInventoryItem = GetWorld()->SpawnActor<ATPInventoryItem>(TPInventoryItemClass, Parameters);
	}
}

void AInventoryItem::ShowItemMesh()
{
	if(ItemMeshComponent)
	{
		ItemMeshComponent->SetVisibility(true);
	}
}

void AInventoryItem::HideItemMesh()
{
	if (ItemMeshComponent)
	{
		ItemMeshComponent->SetVisibility(false);
	}
}

