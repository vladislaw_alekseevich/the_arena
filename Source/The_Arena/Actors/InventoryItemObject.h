// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/CanvasRenderTarget2D.h"

#include "The_Arena/UObjects/DefaultDelegates.h"
#include "The_Arena/UObjects/InventoryItemParams/InventoryItemParam.h"

#include "InventoryItemObject.generated.h"


class UInventoryItemCommonParams;
class ADroppedItem;
class ATPInventoryItem;

UCLASS()
class THE_ARENA_API AInventoryItemObject : public AActor
{
	GENERATED_BODY()

	//Methods
public:	
	// Sets default values for this actor's properties
	AInventoryItemObject();
	virtual void Initialization(const UInventoryItemParam* InInventoryItemParam);
	
	UFUNCTION(Server, Reliable, BlueprintCallable)
		void ServerRotate();
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
		FIntPoint GetDimensions();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		UMaterialInterface* GetIcon();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsRotated() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
		UInventoryItemCommonParams* GetInventoryItemCommonParams() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
		float GetItemWeight() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
		float GetCurrentItemWeight() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		int GetMaxItemAmount() const;
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
		int GetCurrentItemAmount() const;
	UFUNCTION(BlueprintCallable)
		void SetCurrentItemAmount(int Amount);
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
		int GetItemCost() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
		int GetCurrentItemCost() const;
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
		FText GetItemDescription() const;

	virtual UInventoryItemParam* GetInventoryItemParam();
	
	bool TryIncreaseItemAmount(AInventoryItemObject* InInventoryItemObject);
	bool TryRemoveItems(int Amount);

	template < class T >
	T* GetInventoryItemCommonParams() const
	{
		return Cast<T>(GetInventoryItemCommonParams());
	}

	FDMD& GetRotateDispatcher();
	FDMD& GetCurrentItemAmountDispatcher();
	FDMD& GetCurrentItemCostDispatcher();
	FDMD& GetCurrentItemWeightDispatcher();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void DynamicIconsInitialization();

	UFUNCTION()
	void UpdateDynamicIcon();
	void CaptureDynamicIcon(USceneCaptureComponent2D* SceneCapture, ATPInventoryItem* TPInventoryItem);
	
	UFUNCTION()
	void CalculateCurrentItemWeight();
	UFUNCTION()
	void CalculateCurrentItemCost();

	virtual void CreateInventoryItemParam();
	
	UFUNCTION()
	void OnRep_Rotated();
	UFUNCTION()
	void OnRep_CurrentItemAmount();
	UFUNCTION()
	void OnRep_CurrentItemCost();
	UFUNCTION()
	void OnRep_CurrentItemWeight();
	
	//Fields
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FIntPoint Dimensions;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category= "Icon")
	UMaterialInterface* Icon;
	UPROPERTY(EditDefaultsOnly, Category = "Icon")
	UMaterialInterface* IconRotated;
	UPROPERTY(EditDefaultsOnly, Category = "Icon")
	bool bDynamicIcon = false;
	UPROPERTY(EditDefaultsOnly)
	UInventoryItemCommonParams* InventoryItemCommonParams;
	UPROPERTY(EditDefaultsOnly)
	int MaxItemAmount = 1;
	UPROPERTY(EditDefaultsOnly)
	float ItemWeight;
	UPROPERTY(EditDefaultsOnly)
	int ItemCost;
	UPROPERTY(EditDefaultsOnly)
	FText ItemDescription;
	
	UPROPERTY(ReplicatedUsing = OnRep_Rotated)
	bool bRotated = false;
	UPROPERTY(ReplicatedUsing = OnRep_CurrentItemAmount)
	int CurrentItemAmount = 1;
	UPROPERTY(ReplicatedUsing = OnRep_CurrentItemCost)
	int CurrentItemCost;
	UPROPERTY(ReplicatedUsing = OnRep_CurrentItemWeight)
	float CurrentItemWeight;
	
	UPROPERTY()
	UInventoryItemParam* InventoryItemParam;

	UPROPERTY()
	UMaterialInstanceDynamic* DynamicIcon;
	UPROPERTY()
	UMaterialInstanceDynamic* DynamicIconRotated;
	UPROPERTY()
	UCanvasRenderTarget2D* ItemCanvasRenderTarget;
	
	//Events
	UPROPERTY(BlueprintAssignable)
	FDMD RotateDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD CurrentItemAmountDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD CurrentItemCostDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD CurrentItemWeightDispatcher;
};
