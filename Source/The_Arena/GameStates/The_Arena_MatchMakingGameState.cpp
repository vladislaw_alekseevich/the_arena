// Fill out your copyright notice in the Description page of Project Settings.


#include "The_Arena_MatchMakingGameState.h"

#include "The_Arena/PlayerStates/The_Arena_PlayerState.h"
#include "The_Arena/GameMods/The_Arena_MatchMakingGameMode.h"

void AThe_Arena_MatchMakingGameState::AddPlayerStateInTeam(EGameTeam Team, APlayerState* PlayerState)
{
	switch (Team)
	{
	case EGameTeam::Team_1:
		Team_1.Add(PlayerState);
		break;
	case EGameTeam::Team_2:
		Team_2.Add(PlayerState);
		break;
	case EGameTeam::Spectators:
		Spectators.Add(PlayerState);
		break;
	default:
		break;
	}
}

void AThe_Arena_MatchMakingGameState::RemovePlayerStateFromTeam(EGameTeam Team, APlayerState* PlayerState)
{
	switch (Team)
	{
	case EGameTeam::Team_1:
		if (Team_1.Contains(PlayerState))
		{
			Team_1.Remove(PlayerState);
		}
		break;
	case EGameTeam::Team_2:
		if (Team_2.Contains(PlayerState))
		{
			Team_2.Remove(PlayerState);
		}
		break;
	case EGameTeam::Spectators:
		if (Spectators.Contains(PlayerState))
		{
			Spectators.Remove(PlayerState);
		}
		break;
	default:
		break;
	}
}

void AThe_Arena_MatchMakingGameState::AddPlayerStateInAlivePlayersTeam(APlayerState* PlayerState)
{	
	if(auto ArenaPlayerState = Cast<AThe_Arena_PlayerState>(PlayerState))
	{
		EGameTeam Team = ArenaPlayerState->GetCurrentTeam();

		switch (Team) {
			case EGameTeam::Team_1:
				AlivePlayersInTeam_1.Add(PlayerState);
				if(GetLocalRole() == ROLE_Authority)
				{
					OnRep_PlayerAliveInTeam_1();
				}
				break;
			case EGameTeam::Team_2:
				AlivePlayersInTeam_2.Add(PlayerState);
				if (GetLocalRole() == ROLE_Authority)
				{
					OnRep_PlayerAliveInTeam_2();
				}
				break;
			default:
				break;
		}
	}
}

void AThe_Arena_MatchMakingGameState::RemovePlayerStateFromAlivePlayersTeam(APlayerState* PlayerState)
{
	if (auto ArenaPlayerState = Cast<AThe_Arena_PlayerState>(PlayerState))
	{
		EGameTeam Team = ArenaPlayerState->GetCurrentTeam();

		switch (Team) {
		case EGameTeam::Team_1:
			AlivePlayersInTeam_1.Remove(PlayerState);
			if (GetLocalRole() == ROLE_Authority)
			{
				OnRep_PlayerAliveInTeam_1();
			}
			break;
		case EGameTeam::Team_2:
			AlivePlayersInTeam_2.Remove(PlayerState);
			if (GetLocalRole() == ROLE_Authority)
			{
				OnRep_PlayerAliveInTeam_2();
			}
			break;
		default:
			break;
		}
	}
}

void AThe_Arena_MatchMakingGameState::RemovePlayerStateFromAlivePlayersTeam_Cover(AController* Controller)
{
	if (!Controller)
		return;

	if(auto PlayerState = Controller->GetPlayerState<APlayerState>())
	{
		RemovePlayerStateFromAlivePlayersTeam(PlayerState);
	}
}

const TArray<APlayerState*> AThe_Arena_MatchMakingGameState::GetTeam_1()
{
	return Team_1;
}

const TArray<APlayerState*> AThe_Arena_MatchMakingGameState::GetTeam_2()
{
	return Team_2;
}

const TArray<APlayerState*> AThe_Arena_MatchMakingGameState::GetSpectators()
{
	return Spectators;
}

const TArray<APlayerState*> AThe_Arena_MatchMakingGameState::GetAlivePlayersInTeam_1()
{
	return AlivePlayersInTeam_1;
}

const TArray<APlayerState*> AThe_Arena_MatchMakingGameState::GetAlivePlayersInTeam_2()
{
	return AlivePlayersInTeam_2;
}

int AThe_Arena_MatchMakingGameState::GetBuyingStateDuration() const
{
	return BuyingStateDuration;
}

void AThe_Arena_MatchMakingGameState::SetBuyingStateDuration(int Duration)
{
	BuyingStateDuration = Duration;
}

int AThe_Arena_MatchMakingGameState::GetRoundDuration() const
{
	return RoundDuration;
}

void AThe_Arena_MatchMakingGameState::SetRoundDuration(int Duration)
{
	RoundDuration = Duration;
}

int AThe_Arena_MatchMakingGameState::GetEndMatchWithoutPlayersDuration() const
{
	return EndMatchWithoutPlayersDuration;
}

void AThe_Arena_MatchMakingGameState::SetEndMatchWithoutPlayersDuration(int Duration)
{
	EndMatchWithoutPlayersDuration = Duration;
}

int AThe_Arena_MatchMakingGameState::GetBuyingStateTimePassed() const
{
	return BuyingStateTimePassed;
}

void AThe_Arena_MatchMakingGameState::SetBuyingStateTimePassed(int TimePassed)
{
	BuyingStateTimePassed = TimePassed;

	if (GetLocalRole() == ROLE_Authority)
	{
		OnRep_BuyingStateTimePassed();
	}
}

int AThe_Arena_MatchMakingGameState::GetWonRounds_Team_1() const
{
	return WonRounds_Team_1;
}

void AThe_Arena_MatchMakingGameState::SetWonRounds_Team_1(int WonRounds)
{
	WonRounds_Team_1 = WonRounds;

	if(GetLocalRole() == ROLE_Authority)
	{
		OnRep_WonRounds_Team_1();
	}
}

int AThe_Arena_MatchMakingGameState::GetWonRounds_Team_2() const
{
	return WonRounds_Team_2;
}

void AThe_Arena_MatchMakingGameState::SetWonRounds_Team_2(int WonRounds)
{
	WonRounds_Team_2 = WonRounds;

	if (GetLocalRole() == ROLE_Authority)
	{
		OnRep_WonRounds_Team_2();
	}
}

bool AThe_Arena_MatchMakingGameState::IsGameStarted() const
{
	return bGameStarted;
}

void AThe_Arena_MatchMakingGameState::SetGameStarted(bool bInGameStarted)
{
	bGameStarted = bInGameStarted;
}

bool AThe_Arena_MatchMakingGameState::IsGameOver() const
{
	return bGameOver;
}

void AThe_Arena_MatchMakingGameState::SetGameOver(bool bInGameOver)
{
	bGameOver = bInGameOver;
}

int AThe_Arena_MatchMakingGameState::GetEndMatchWithoutPlayersTimePassed() const
{
	return EndMatchWithoutPlayersTimePassed;
}

void AThe_Arena_MatchMakingGameState::SetEndMatchWithoutPlayersTimePassed(int TimePassed)
{
	EndMatchWithoutPlayersTimePassed = TimePassed;

	if (GetLocalRole() == ROLE_Authority)
	{
		OnRep_EndMatchWithoutPlayersTimePassed();
	}
}

int AThe_Arena_MatchMakingGameState::GetRestartServerDuration() const
{
	return RestartServerDuration;
}

void AThe_Arena_MatchMakingGameState::SetRestartServerDuration(int Duration)
{
	RestartServerDuration = Duration;
}

int AThe_Arena_MatchMakingGameState::GetRestartServerTimePassed() const
{
	return RestartServerTimePassed;
}

void AThe_Arena_MatchMakingGameState::SetRestartServerTimePassed(int TimePassed)
{
	RestartServerTimePassed = TimePassed;

	if (GetLocalRole() == ROLE_Authority)
	{
		OnRep_RestartServerTimePassed();
	}
}

EGameTeam AThe_Arena_MatchMakingGameState::GetWonMatchTeam() const
{
	return WonMatchTeam;
}

void AThe_Arena_MatchMakingGameState::SetWonMatchTeam(EGameTeam InWonTeam)
{
	WonMatchTeam = InWonTeam;
}

EGameTeam AThe_Arena_MatchMakingGameState::GetWonRoundTeam() const
{
	return WonRoundTeam;
}

void AThe_Arena_MatchMakingGameState::SetWonRoundTeam(EGameTeam InWonTeam)
{
	WonRoundTeam = InWonTeam;
}

EGameTeam AThe_Arena_MatchMakingGameState::GetLastWonRoundTeam() const
{
	return LastWonRoundTeam;
}

void AThe_Arena_MatchMakingGameState::SetLastWonRoundTeam(EGameTeam InWonTeam)
{
	LastWonRoundTeam = InWonTeam;
}

FDMD& AThe_Arena_MatchMakingGameState::GetWonRounds_Team_1_Dispatcher()
{
	return WonRounds_Team_1_Dispatcher;
}

FDMD& AThe_Arena_MatchMakingGameState::GetWonRounds_Team_2_Dispatcher()
{
	return WonRounds_Team_2_Dispatcher;
}

FDMD& AThe_Arena_MatchMakingGameState::GetBuyingStateTimePassedDispatcher()
{
	return BuyingStateTimePassedDispatcher;
}

FDMD& AThe_Arena_MatchMakingGameState::GetEndMatchWithoutPlayersTimePassedDispatcher()
{
	return EndMatchWithoutPlayersTimePassedDispatcher;
}

FDMD& AThe_Arena_MatchMakingGameState::GetElapsedTimeDispatcher()
{
	return ElapsedTimeDispatcher;
}

FDMD& AThe_Arena_MatchMakingGameState::GetRestartServerTimePassedDispatcher()
{
	return RestartServerTimePassedDispatcher;
}

FDMD& AThe_Arena_MatchMakingGameState::GetMatchStateDispatcher()
{
	return MatchStateDispatcher;
}

FDMD& AThe_Arena_MatchMakingGameState::GetAlivePlayersInTeam_1_Dispatcher()
{
	return AlivePlayersInTeam_1_Dispatcher;
}

FDMD& AThe_Arena_MatchMakingGameState::GetAlivePlayersInTeam_2_Dispatcher()
{
	return AlivePlayersInTeam_2_Dispatcher;
}

void AThe_Arena_MatchMakingGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AThe_Arena_MatchMakingGameState, BuyingStateDuration);
	DOREPLIFETIME(AThe_Arena_MatchMakingGameState, BuyingStateTimePassed);
	DOREPLIFETIME(AThe_Arena_MatchMakingGameState, RoundDuration);
	DOREPLIFETIME(AThe_Arena_MatchMakingGameState, EndMatchWithoutPlayersDuration);
	DOREPLIFETIME(AThe_Arena_MatchMakingGameState, EndMatchWithoutPlayersTimePassed);
	DOREPLIFETIME(AThe_Arena_MatchMakingGameState, RestartServerDuration);
	DOREPLIFETIME(AThe_Arena_MatchMakingGameState, RestartServerTimePassed);

	DOREPLIFETIME(AThe_Arena_MatchMakingGameState, Team_1);
	DOREPLIFETIME(AThe_Arena_MatchMakingGameState, Team_2);
	DOREPLIFETIME(AThe_Arena_MatchMakingGameState, Spectators);
	DOREPLIFETIME(AThe_Arena_MatchMakingGameState, WonRounds_Team_1);
	DOREPLIFETIME(AThe_Arena_MatchMakingGameState, WonRounds_Team_2);
	DOREPLIFETIME(AThe_Arena_MatchMakingGameState, bGameStarted);
	DOREPLIFETIME(AThe_Arena_MatchMakingGameState, bGameOver);
	DOREPLIFETIME(AThe_Arena_MatchMakingGameState, WonMatchTeam);
	DOREPLIFETIME(AThe_Arena_MatchMakingGameState, WonRoundTeam);
	DOREPLIFETIME(AThe_Arena_MatchMakingGameState, LastWonRoundTeam);
	DOREPLIFETIME(AThe_Arena_MatchMakingGameState, AlivePlayersInTeam_1);
	DOREPLIFETIME(AThe_Arena_MatchMakingGameState, AlivePlayersInTeam_2);
}

void AThe_Arena_MatchMakingGameState::DefaultTimer()
{
	if(GetLocalRole() == ROLE_Authority)
	{
		if (GetMatchState() == MatchState::BuyingState)
		{
			SetBuyingStateTimePassed(BuyingStateTimePassed + 1);
		}

		if (auto ArenaGameMode = Cast<AThe_Arena_MatchMakingGameMode>(AuthorityGameMode))
		{
			if (GetMatchState() == MatchState::EndRound && ArenaGameMode->IsGameOver())
			{
				SetRestartServerTimePassed(RestartServerTimePassed + 1);
			}

			if (ArenaGameMode->IsEndMatchWithoutPlayersTimerEnabled())
			{
				if (EndMatchWithoutPlayersTimePassed < ArenaGameMode->GetEndMatchWithoutPlayersDuration())
				{
					SetEndMatchWithoutPlayersTimePassed(EndMatchWithoutPlayersTimePassed + 1);
				}
			}
			else
			{
				if(EndMatchWithoutPlayersTimePassed != 0)
				{
					SetEndMatchWithoutPlayersTimePassed(0);
				}			
			}
		}
	}	
	
	Super::DefaultTimer();
}

void AThe_Arena_MatchMakingGameState::OnRep_WonRounds_Team_1()
{
	WonRounds_Team_1_Dispatcher.Broadcast();
}

void AThe_Arena_MatchMakingGameState::OnRep_WonRounds_Team_2()
{
	WonRounds_Team_2_Dispatcher.Broadcast();
}

void AThe_Arena_MatchMakingGameState::OnRep_BuyingStateTimePassed()
{
	BuyingStateTimePassedDispatcher.Broadcast();
}

void AThe_Arena_MatchMakingGameState::OnRep_EndMatchWithoutPlayersTimePassed()
{
	EndMatchWithoutPlayersTimePassedDispatcher.Broadcast();
}

void AThe_Arena_MatchMakingGameState::OnRep_ElapsedTime()
{
	Super::OnRep_ElapsedTime();
	ElapsedTimeDispatcher.Broadcast();
}

void AThe_Arena_MatchMakingGameState::OnRep_MatchState()
{
	Super::OnRep_MatchState();
	MatchStateDispatcher.Broadcast();
}

void AThe_Arena_MatchMakingGameState::OnRep_RestartServerTimePassed()
{
	RestartServerTimePassedDispatcher.Broadcast();
}

void AThe_Arena_MatchMakingGameState::OnRep_PlayerAliveInTeam_1()
{
	AlivePlayersInTeam_1_Dispatcher.Broadcast();
}

void AThe_Arena_MatchMakingGameState::OnRep_PlayerAliveInTeam_2()
{
	AlivePlayersInTeam_2_Dispatcher.Broadcast();
}
