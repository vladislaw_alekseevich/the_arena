// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

#include "The_Arena/Actors/InventoryItem.h"

#include "InventoryItemsManager.generated.h"

class AInventoryItemObject;

USTRUCT()
struct FInventoryItemParams
{
	GENERATED_BODY()
	
	UPROPERTY()
	AInventoryItemObject* ItemObject = nullptr;
	UPROPERTY()
	bool bTestItem = false;
};

UCLASS(Blueprintable)
class THE_ARENA_API UInventoryItemsManager : public UObject
{
	GENERATED_BODY()

	//Methods
public:
	AInventoryItem* CreateInventoryItem(UClass* Class, const FActorSpawnParameters& SpawnParameters = FActorSpawnParameters(), const FInventoryItemParams& ItemParams = FInventoryItemParams());
};
