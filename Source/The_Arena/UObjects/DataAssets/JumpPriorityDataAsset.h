// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "JumpPriorityDataAsset.generated.h"

UENUM(BlueprintType)
enum EAbility
{
	NONE, Can, Cant
};

USTRUCT(BlueprintType)
struct FJumpPriorityParam
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly)
	FString Name;
	UPROPERTY(EditDefaultsOnly)
	int Priority;
	UPROPERTY(EditDefaultsOnly)
	TEnumAsByte<EAbility> Ability;
};

UCLASS()
class THE_ARENA_API UJumpPriorityDataAsset : public UDataAsset
{
	GENERATED_BODY()

		//Methods
public:
	UFUNCTION(BlueprintCallable)
	FJumpPriorityParam GetPriority(FString Name) const;
	//Fields
protected:
	UPROPERTY(EditDefaultsOnly)
		TArray<FJumpPriorityParam> Priorities;
};
