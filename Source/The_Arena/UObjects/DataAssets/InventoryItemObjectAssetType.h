// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"

#include "The_Arena/Actors/InventoryItemObject.h"

#include "InventoryItemObjectAssetType.generated.h"

USTRUCT()
struct FInventoryItemObjectAssetStruct
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly)
	FName ItemObjectName;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AInventoryItemObject> ItemObjectClass;
};

/**
 * 
 */
UCLASS()
class THE_ARENA_API UInventoryItemObjectAssetType : public UDataAsset
{
	GENERATED_BODY()

	//Methods
public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	UClass* GetInventoryItemObjectClass(FName ItemObjectName);
	
	//Fields
protected:
	UPROPERTY(EditDefaultsOnly)
	TArray<FInventoryItemObjectAssetStruct> InventoryItemClasses;
};
