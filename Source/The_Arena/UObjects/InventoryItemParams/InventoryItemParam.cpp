// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryItemParam.h"

void UInventoryItemParam::CopyTo(UInventoryItemParam* InventoryItemParam) const
{
	if (!InventoryItemParam)
		return;

	InventoryItemParam->ItemAmount = ItemAmount;
	InventoryItemParam->ModuleItemObjects = ModuleItemObjects;
}
