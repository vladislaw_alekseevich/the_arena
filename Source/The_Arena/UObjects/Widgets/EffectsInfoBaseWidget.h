// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "The_Arena/ActorComponents/Effects/CharacteristicEffectBase.h"

#include "EffectsInfoBaseWidget.generated.h"


UCLASS()
class THE_ARENA_API UEffectsInfoBaseWidget : public UUserWidget
{
	GENERATED_BODY()

	//Methods
public:
	UFUNCTION(BlueprintImplementableEvent)
	void CreateEffectInfoWidget(FName EffectName, float UsingTime, ECharacteristicEffectType EffectType);
	UFUNCTION(BlueprintImplementableEvent)
	void RemoveEffectInfoWidget(FName EffectName);
};
